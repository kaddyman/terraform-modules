variable emails { default = {} }
variable ses_region { default = "us-east-1" }
variable account_id {}

provider "aws" {
  alias  = "ses"
  region = var.ses_region

  assume_role {
    role_arn     = "arn:aws:iam::${var.account_id}:role/terralign" # change account number to target
    session_name = "terralign"
    external_id  = "terralign_id"
  }
}

resource "aws_ses_email_identity" "self" {
  provider = aws.ses
  for_each = var.emails
  email    = each.key
}

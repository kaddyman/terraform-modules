# ses-emails

This module will add emails to us-east-1 ses so we can send emails to them.


This module should be called from global since it's region dependant not vpc. 
Should also note due to the limits of ses region availability we don't create 
a new region in terralign for this and just use a alias provider for it.


If you want to import emails already in aws you will need to add a new provider 
with a alias of `ses` and region of `us-east-1`. When you run terraform use the 
`-provider=aws.ses` option on the import or it will fail with region errors.

### inputs
- emails (map): map of emails to be added to ses
- account_id (string): AWS account id of where you are adding emails
- ses_region (string, default=us-east-1): ses region we default to us-east-1

### outputs
N/A

### example
```
module "ses_emails" {
  source = "../../../../modules/aws/ses-emails"

  account_id = local.account_id
  ses_region = "us-east-1"
  emails     = {
    "ken@addyman.io" = "ken.addyman",
  }
}
```

# dynamo-db

This module creates a dynamo-db table

### inputs
- table_name (string): name of the table to be created
- ttl (object, default=null): using this object will enable ttl on this table and requires:
    -  attribute_name (string): which attribute to apply ttl to
- read_capacity (number): read capacity of this table
- write_capacity (number): write capacity of this table
- hash_key_name (string): name of the hash key for this table
- hash_key_type (string): type of the hash key for this table
- backup_enabled (bool, default=false): enable backups for this table

### outputs
- table_name_id (string): name of the table
- table_name_arn (string): arn of the table

### example
```
module "ip2geo_table" {
  source         = "../../../modules/aws/dynamo-db"
  table_name     = "ip2geo"
  read_capacity  = 20
  write_capacity = 20
  hash_key_name  = "ip_address"
  hash_key_type  = "S"
}
```


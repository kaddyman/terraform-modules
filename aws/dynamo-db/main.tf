variable table_name {}
variable read_capacity {}
variable write_capacity {}
variable hash_key_name {}
variable hash_key_type {}
variable ttl {
  type = object({
    attribute_name = string
  })
  default = null
}
variable backup_enabled {
  type    = bool
  default = false
}

locals {
  default_tags = {
    terralign   = "true"
  }
}

resource "aws_dynamodb_table" "self" {
  name           = var.table_name
  read_capacity  = var.read_capacity
  write_capacity = var.write_capacity
  hash_key       = var.hash_key_name

  tags = local.default_tags

  attribute {
    name = var.hash_key_name
    type = var.hash_key_type
  }

  dynamic "ttl" {
    for_each = var.ttl == null ? [] : [1]
    content {
      enabled        = true
      attribute_name = var.ttl.attribute_name
    }
  }

  point_in_time_recovery {
    enabled = var.backup_enabled
  }
}

output "table_name_id" {
  value = aws_dynamodb_table.self.id
}

output "table_name_arn" {
  value = aws_dynamodb_table.self.arn
}


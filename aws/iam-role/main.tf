variable identifiers {}
variable role_name {}
variable path { default = "/" }
variable policies { default = [] }
variable attach { default = null }
variable role_map { default = {} }
variable max_session_duration { default = 3600 }
variable principal_type { default = "AWS" }
variable role_condition { default = null }

locals {
  default_tags = {
    type        = "iam"
    terraform   = "true"
  }
}

resource "aws_iam_role" "self" {
  name                 = var.role_name
  assume_role_policy   = data.aws_iam_policy_document.self.json
  max_session_duration = var.max_session_duration 
  path                 = var.path

  tags = merge(local.default_tags, var.role_map)
}

data "aws_iam_policy_document" "self" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = var.principal_type
      identifiers = var.identifiers
    }
    dynamic "condition" {
      for_each = var.role_condition == null ? [] : var.role_condition

      content {
        test     = condition.value.test
        variable = condition.value.variable
        values   = condition.value.values
      }
    }
  }
}

data "aws_iam_policy_document" "self_policy" {
  count = length(var.policies)
  dynamic "statement" {
    for_each = [for s in var.policies[count.index].statements : {
      effect     = lookup(s, "effect", "Allow")
      actions    = lookup(s, "actions", null)
      resources  = lookup(s, "resources", null)
      sid        = lookup(s, "sid", null)
      condition  = lookup(s, "condition", [])
    }]

    content {
      sid       = statement.value.sid
      effect    = statement.value.effect
      actions   = statement.value.actions
      resources = statement.value.resources

      dynamic "condition" {
        for_each = statement.value.condition
    
        content {
          test     = condition.value.test
          variable = condition.value.variable
          values   = condition.value.values 
        }
      }
    }
  }
}

resource "aws_iam_policy" "self_policy" {
  count  = length(var.policies)
  name   = var.policies[count.index].policy_name
  policy = data.aws_iam_policy_document.self_policy[count.index].json
}

resource "aws_iam_role_policy_attachment" "self_policy" {
  count      = length(var.policies)
  role       = aws_iam_role.self.name
  policy_arn = aws_iam_policy.self_policy[count.index].arn
}

resource "aws_iam_role_policy_attachment" "attach" {
  count      = var.attach == null ? 0 : length(var.attach)
  role       = aws_iam_role.self.name
  policy_arn = var.attach[count.index]
}

output "role_arn" {
  value = aws_iam_role.self.arn
}

output "role_id" {
  value = aws_iam_role.self.id
}

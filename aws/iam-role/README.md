# iam-role

This module will create a role with n policies that will be used as assumed roles 
in aws accounts. This module is ONLY for iam users/groups and NOT applications.

Notes:

The role by default comes with a sts:AssumeRole.

### inputs
- island (string): the island (account short name) that the resources will live in
- cluster (string): the cluster (vpc name) that the resources will live in
- lob (string): line of business this group belongs to
- product (string): product
- owner (string): owner
- role_name (string): name of your role
- path (string, default = "/"): path for your role, you don't need this 99% of the time
- max_session_duration (int, default = 3600): the length of time (secs) creds from this role will last. default 1 hour (3600s), max 12 hours (43200s)
- role_map (map, default = {}): additional tags to add to the role
- identifiers (list) arn's to use for your assume role policy
- attach (list): optional, attach already existing polices to the role via the arn
- policies (list(object{}), default = []): a list of maps that contain one policy each with n statement blocks
- principal_type (string, default = "AWS"): Either AWS (IAM user or role ARNs) OR Service (these are AWS Service roles)
- role_condition (list, default = null): add conditions to your role trust relationship like `sts:ExternalId`

### outputs
- role_arn: arn of the role
- role_id: id of the role

### example
```
module "role_testing" {
  source = "../../../modules/aws/iam-role"

  island      = local.island
  cluster     = local.cluster
  product     = "infra"
  application = "iam"
  owner       = "devops"
  lob         = "kenny"

  role_name   = "testy-stuff"
  identifiers = ["arn:aws:iam::${data.aws_caller_identity.control.account_id}:root"]
  role_condition = [{ 
    test = "StringEquals", 
    variable = "sts:ExternalId", 
    values = ["0000"]
  }]

  role_map = {
    restart = "true"
  }

  attach = [
    "arn:aws:iam::aws:policy/AmazonElastiCacheReadOnlyAccess",
    "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
  ]

  policies = [
    {
      policy_name = "multi-statement-with-condition",
      statements = [{
        actions    = ["s3:ListBucket"],
        resources  = ["arn:aws:s3:::lol-other-bucket"],
        condition  = [
          { test = "StringLike", variable = "s3:prefix", values = ["home/"]}
        ]
      },
      {
        actions     = [ "s3:GetObject", "s3:PutObject" ],
        resources   = ["arn:aws:s3:::lol-other-bucket/*"]
      }]
    },
    {
      policy_name = "normal-policy-with-sid",
      statements = [{
        sid         = "TestingPolicy"
        actions     = [ "s3:*Object" ],
        resources   = ["arn:aws:s3:::lol-bucket/*"]
      }]
    }
  ]
}
```

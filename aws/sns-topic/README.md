# sns-topic

This module will create an sns topic and optionally a topic policy. 
It can also create subscritions to the topic.

SNS belongs in realm as it is tied to a region rather than a vpc.

### inputs
- topic_name (string): the name of the topic
- has_policy (bool, default=false) denotes if there will be a policy attached to the topic.  If false or absent, it will only create a topic.  If true it will create a topic and a policy for that topic
- topic_policy (string, default=null): the fully-formed AWS policy as JSON. For more information about building AWS IAM policy documents with Terraform, see the AWS IAM Policy Document Guide. Policy Doc: 'https://www.terraform.io/docs/providers/aws/guides/iam-policy-documents.html'.
- subscriptions (map, default = {}): map defining the subscriptions this topic has, email protocol is not supported

### outputs
- sns_arn (string): sns topic arn

### example without policy
```
module "sns_topic_test" {
  source = "../../../modules/aws/sns-topic"
  topic_name   = "test-topic-name"
}
```

### example with policy
```
module "sns_topic_test" {
  source = "../../../modules/aws/sns-topic"

  topic_name   = "test-topic-name"

  subscriptions = {
    rti-lambda = {
      protocol               = "lambda"
      endpoint               = "arn:aws:lambda:us-east-2:054706986272:function:rti-vehicle-cleanup"
      endpoint_auto_confirms = true
      raw_message_delivery   = false
    },
    rti-lambda-2 = {
      protocol               = "lambda"
      endpoint               = "arn:aws:lambda:us-east-2:054706986272:function:rti-vehicle-cleanup"
      endpoint_auto_confirms = true
      raw_message_delivery   = false
    }
  }

  has_policy   = "true"
  topic_policy = <<EOF
{
  "Version": "2008-10-17",
  "Id": "snsNotify",
  "Statement": [
    {
      "Sid": "s3sync",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "SNS:Publish",
      "Resource": "arn:aws:sns:us-east-2:055676811907:s3-file-sync",
      "Condition": {
        "ArnLike": {
          "aws:SourceArn": "arn:aws:s3:*:*:autoniq-ftp-sync"
        }
      }
    }
  ]
}
EOF
}
```

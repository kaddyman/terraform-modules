variable topic_name {}
variable topic_policy { default = null }
variable has_policy {
  type    = bool
  default = false
}
variable subscriptions {default = {} }

locals {
  default_tags = {
    terralign     = "true"
  }
}

resource "aws_sns_topic" "self" {
  name = var.topic_name

  tags = local.default_tags
}

output "sns_arn" {
  value = aws_sns_topic.self.arn
}

resource "aws_sns_topic_policy" "self" {
  count  = var.has_policy == true ? 1 : 0
  arn    = aws_sns_topic.self.arn
  policy = var.topic_policy
}

resource "aws_sns_topic_subscription" "self" {
  for_each = var.subscriptions
  topic_arn = aws_sns_topic.self.arn
  protocol  = each.value["protocol"]
  endpoint  = each.value["endpoint"]
  endpoint_auto_confirms = each.value["endpoint_auto_confirms"]
  raw_message_delivery = each.value["raw_message_delivery"]
}

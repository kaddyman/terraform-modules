variable website {}
variable lambda_bucket {}

locals {
  default_tags = {
    terraform = "true"
  }
}

resource "aws_lambda_function" "self" {
  function_name    = "http-security-headers-${var.website}"
  handler          = "index.handler"
  s3_bucket        = var.lambda_bucket
  s3_key           = "http-security-headers-${var.website}/http-security-headers-${var.website}.zip"
  publish          = true
  role             = aws_iam_role.self.arn
  runtime          = "nodejs10.x"
  memory_size      = 128
  timeout          = 3

  tags = local.default_tags
}

output "qualified_arn" {
  value = aws_lambda_function.self.qualified_arn
}

resource "aws_iam_role" "self" {
  name               = "http-security-headers-${var.website}"
  path               = "/service-role/"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "lambda.amazonaws.com",
          "edgelambda.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = local.default_tags
}

data "aws_iam_policy_document" "self_cloudwatach_logs" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]
    resources = ["arn:aws:logs:*:*:*"]
  }
}

resource "aws_iam_policy" "self_cloudwatach_logs" {
  name   = "http-security-headers-${var.website}-logs"
  policy = data.aws_iam_policy_document.self_cloudwatach_logs.json
}

resource "aws_iam_role_policy_attachment" "self_cloudwatach_logs" {
  role       = aws_iam_role.self.name
  policy_arn = aws_iam_policy.self_cloudwatach_logs.arn
}


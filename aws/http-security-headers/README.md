# http-security-headers

This will spin up a Lambda@Edge that you can attach to a cloudfront distribuntion


Useful if you want to attach additional headers like the HTTP security headers 
so you can get a A+ rating on your site.


To pushlish a new version have your zip/sha file uploaded to the lambdas buckets 
and rerun this module to update lambda version that your cloudfront distro 
points at.

### inputs
- island (string): the island (account short name) that the resources will live in
- cluster (string): the cluster (vpc name) that the resources will live in
- cluster (string): the cluster (vpc name) that the resources will live in
- website (string): website name with hyphens
- lambda_bucket (string): s3 bucket that holds you lambda code zip 
 
### outputs
- qualified_arn (string): arn that can be passed to cloudfront

### example
```
module "http_security_headers_belial_io" {
  source  = "../../../modules/aws/http-security-headers"

  website       = "belial-io"
  lambda_bucket = module.kac_lambdas.bucket_id
}
```

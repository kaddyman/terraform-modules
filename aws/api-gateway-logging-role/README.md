# api-gateway-logging-role

This will create the required role for api-gateway so APIs can log to cloudwatch.

Should only be called in global.

### inputs
- N/A
 
### outputs
- N/A

### example
```
module "api_gateway_logging_role" {
  source  = "../../../modules/aws/api-gateway-logging-role"
}
```

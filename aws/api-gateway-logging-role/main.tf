locals {
  default_tags = {
    terraform = "true"
  }
}

resource "aws_api_gateway_account" "self" {
  cloudwatch_role_arn = aws_iam_role.self.arn
}

resource "aws_iam_role" "self" {
  name = "api-gateway-cloudwatch-global"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = local.default_tags
}

data "aws_iam_policy_document" "self" {
  statement {
    sid = "ApiGatewayAllowCloudWatchLogs"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:GetLogEvents",
      "logs:FilterLogEvents"
    ]
    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "self" {
  name   = "api-gateway-cloudwatch-global"
  policy = data.aws_iam_policy_document.self.json
}

resource "aws_iam_role_policy_attachment" "self" {
  role       = aws_iam_role.self.name
  policy_arn = aws_iam_policy.self.arn
}

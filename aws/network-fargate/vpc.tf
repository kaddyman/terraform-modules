data "aws_caller_identity" "current" {}
variable vpc_name {}
variable availability_zones { type = list }
variable vpc_cidr {}

locals {
  az_count = length(var.availability_zones)
  max_azs  = 2

  allowed_networks = [
    "0.0.0.0/32"
  ]

  default_tags = {
    terralign   = "true"
  }

}

resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(local.default_tags, 
    tomap({"Name" = "${var.vpc_name}-vpc"})
  )
}

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "vpc_owner" {
  value = aws_vpc.vpc.owner_id
}

output "vpc_cidr" {
  value = aws_vpc.vpc.cidr_block
}

resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = cidrsubnet(var.vpc_cidr, 8, count.index + 1 + local.max_azs)
  availability_zone = var.availability_zones[count.index % local.max_azs]
  count             = local.az_count

  tags = merge(local.default_tags,
    tomap({
      "Name" = "${var.vpc_name}-private-${element(var.availability_zones, count.index)}",
      "zone" = "private",
      "id"   =  count.index % local.az_count + 1
    })
  )
}

output "private_subnet_ids" {
  value = aws_subnet.private.*.id
}

output "private_subnet_arns" {
  value = aws_subnet.private.*.arn
}

output "private_subnet_cidr" {
  value = aws_subnet.private.*.cidr_block
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.vpc.id

  egress {
    from_port   = 0
    protocol    = -1
    to_port     = 0
    self        = true
    description = "Default security group"
  }

  ingress {
    from_port   = 0
    protocol    = -1
    to_port     = 0
    self        = true
    description = "Default security group"
  }

  tags = merge(local.default_tags,
    tomap({
      "Name" =  "${var.vpc_name}-default"
    })
  )
}

output "sg_default_id" {
  value = aws_default_security_group.default.id
}

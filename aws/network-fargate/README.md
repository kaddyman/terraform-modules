# network-fargate

This module creates a private vpc, 2 subnets, route table entries and setups 
an internet gateway. meant to be used for a cheap vpc for fargate use.

No nat gateway or NLB/ALB/target group needed.

### inputs
- availability_zones (list): list of availability zones the network will be in
- vpc_cidr (string): the cidr block for the vpc

### outputs
- vpc_cidr (str): cidr block of the new vpc
- availability_zones (list): list of AZ's to use in this vpc
- vpc_name (str): name for the vpc

### example
```
module "network" {
  source             = "../../../../modules/aws/network-fargate"
  vpc_name           = "non-prod"
  vpc_cidr           = local.vpc_cidr
  availability_zones = local.availability_zones
}
```

resource "aws_security_group" "corporate" {
  name        = "corporate"
  vpc_id      = aws_vpc.vpc.id
  description = "Security group applied to all resources"

  egress {
    from_port        = 0
    protocol         = -1
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = local.allowed_networks
    description = "Allowed Networks"
  }

  ingress {
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = local.allowed_networks
    description = "Allowed Networks"
  }

  tags = merge(local.default_tags,
    tomap({"Name" = "${var.vpc_name}-corporate"})
  )
}

output "sg_corporate_id" {
  value = aws_security_group.corporate.id
}

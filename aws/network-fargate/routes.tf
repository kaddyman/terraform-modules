resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = merge(local.default_tags,
    tomap({"Name" = "${var.vpc_name}-igw"})
  )
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.vpc.id
  count  = local.az_count

  tags = merge(local.default_tags,
    tomap({
      "Name" = "${var.vpc_name}-private-${element(var.availability_zones, count.index)}",
      "zone" = "private"
    })
  )
}

output "rtb_private" {
  value = aws_route_table.private.*.id
}

resource "aws_route" "private_default" {
  timeouts {
    create = "5m"
  }

  count                  = local.az_count
  route_table_id         = aws_route_table.private.*.id[count.index % local.az_count]
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

resource "aws_route_table_association" "private" {
  count          = local.az_count
  subnet_id      = aws_subnet.private.*.id[count.index]
  route_table_id = aws_route_table.private.*.id[count.index % local.az_count]
}


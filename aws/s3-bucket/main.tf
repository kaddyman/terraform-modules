variable account_id { default = null }
variable bucket_name {}
variable inventories { default = {} }
variable included_object_versions { default = "Current" }
variable inventory_schedule { default = "Daily" }
variable s3_bucket_notification { default = [] }
variable block_public_access {
  type    = bool
  default = true
}
variable bucket_inventory {
  type = bool
  default = false
}
variable inventory_format { default = "CSV" }
variable versioning {
  type    = bool
  default = false
}
variable policy {
  type    = string
  default = null
}
variable logging {
  type = object({
    target_bucket = string
    target_prefix = string
  })
  default = null
}
variable lifecycle_rule { default = [] }
variable cors_rule { default = [] }
variable website {
  type = object({
    index_document = string
    error_document = string
    routing_rules  = string
  })
  default = null
}

locals {
  default_tags = {
    terraform   = "true"
  }
}

resource "aws_s3_bucket" "self" {
  bucket        = var.bucket_name
  acl           = "private"
  force_destroy = false
  policy        = var.policy

  versioning {
    enabled = var.versioning
  }

  dynamic "logging" {
    for_each = var.logging == null ? [] : [1]
    content {
      target_bucket = var.logging.target_bucket
      target_prefix = var.logging.target_prefix
    }
  }

  dynamic "cors_rule" {
    for_each = [for c in var.cors_rule : {
      allowed_headers = lookup(c, "allowed_headers", ["*"])
      allowed_methods = lookup(c, "allowed_methods", ["HEAD", "GET"])
      allowed_origins = lookup(c, "allowed_origins", ["*"])
      max_age_seconds = lookup(c, "max_age_seconds", 3000)
    } if c != length(var.cors_rule)]

   content {
     allowed_headers = cors_rule.value.allowed_headers
     allowed_methods = cors_rule.value.allowed_methods
     allowed_origins = cors_rule.value.allowed_origins
     max_age_seconds = cors_rule.value.max_age_seconds
   }
 }

  dynamic "website" {
    for_each = var.website == null ? [] : [1]
    content {
      index_document = var.website.index_document
      error_document = var.website.error_document
      routing_rules  = var.website.routing_rules
    }
  }

  dynamic "lifecycle_rule" {
    for_each = [for r in var.lifecycle_rule : {
      id         = lookup(r, "id", null)
      prefix     = lookup(r, "prefix", null)
      enabled    = lookup(r, "enabled", null)
      transition = lookup(r, "transition", [])
      expiration = lookup(r, "expiration", [])
      tags       = lookup(r, "tags", null)
    } if r != length(var.lifecycle_rule)]

    content {
      id      = lifecycle_rule.value.id
      prefix  = lifecycle_rule.value.prefix
      enabled = lifecycle_rule.value.enabled

      tags = lifecycle_rule.value.tags == null ? null : lifecycle_rule.value.tags

      dynamic "transition" {
        for_each = [for x in lifecycle_rule.value.transition : {
          days = lookup(x, "days", null)
          storage_class = lookup(x, "storage_class", null)
        } if x != length(lifecycle_rule.value.transition)]
        content {
          days          = transition.value.days
          storage_class = transition.value.storage_class
        }
      }

      dynamic "expiration" {
        for_each = lifecycle_rule.value.expiration 
        
        content {
          days = expiration.value.days
        }
      }
    }
  }

  tags = local.default_tags

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_inventory" "self" {
  for_each = var.inventories
  bucket  = aws_s3_bucket.self.id
  name    = each.key
  enabled = each.value.inventory_enabled

  included_object_versions = var.included_object_versions

  filter {
    prefix = each.value.inventory_prefix
  }

  schedule {
    frequency = var.inventory_schedule
  }

  destination {
    bucket {
      format     = var.inventory_format
      bucket_arn = aws_s3_bucket.self.arn
      account_id = var.account_id
      prefix     = "inventory-manifests/"
      encryption { 
        sse_s3 {}
     }
    }
  }
}

resource "aws_s3_bucket_notification" "self" {
  count = length(var.s3_bucket_notification) > 0 ? 1 : 0
  bucket = aws_s3_bucket.self.id

  dynamic "lambda_function" {
    for_each = [for r in var.s3_bucket_notification : {
      lambda_function_arn = lookup(r, "lambda_function_arn", null)
      events              = lookup(r, "events", ["s3:ObjectCreated:*"])
      id                  = lookup(r, "id", null)
      filter_prefix       = lookup(r, "filter_prefix", null)
      filter_suffix       = lookup(r, "filter_suffix", null)
    }]

    content {
      lambda_function_arn = lambda_function.value.lambda_function_arn
      events              = lambda_function.value.events
      id                  = lambda_function.value.id
      filter_prefix       = lambda_function.value.filter_prefix
      filter_suffix       = lambda_function.value.filter_suffix
    }
  }
}

resource "aws_s3_bucket_public_access_block" "self" {
  count  = var.block_public_access == true ? 1 : 0
  bucket = aws_s3_bucket.self.id

  ignore_public_acls      = true
  restrict_public_buckets = true
  block_public_acls       = true
  block_public_policy     = true
}

output "bucket_id" {
  value = aws_s3_bucket.self.id
}

output "bucket_arn" {
  value = aws_s3_bucket.self.arn
}

output "bucket_regional_domain_name" {
  value = aws_s3_bucket.self.bucket_regional_domain_name
}

# s3-bucket

This module creates a s3 bucket

### inputs
- bucket_name (string): name of the bucket to be created
- account_id (string, default = null): aws account id of the s3 bucket
- inventories (map, default = {} ): map of inventory rules, name, prefix enabled
- included_object_versions (string, default = "Current"): inventory versioned objects "All" or only "Current" objects
- inventory_schedule (string, default = "Daily"): how often to generate the inventory file
- bucket_inventory (string, default = false): add a inventory rule to this buket
- inventory_format (string, default = "CSV"): inventory file format CSV, ORC or Parquet
- versioning (bool, default=false): enable versioning on the bucket
- policy (string, default=null): attach a policy (POLICY string) to the bucket, if any
- lifecycle_rule (list, default = []): list of map with lifecycle config
- s3_bucket_notification (list(map), default = []): s3 bucket notifications/triggers for lambdas
- logging (object, default=null): using this object will enable logging on the bucket and requires:
    - target_bucket (string): bucket id (name) that will be recieving logs
    - target_prefix (string): prefix to store the logs under in the target bucket
- website (object, default=null): using this object will enable static website usage on the bucket and requires:
    - index_document (string): file name of the index page
    - error_document (string): file name of the error page
    - routing_rules (string): routing rules to apply to the bucket
- cors_rule (list(object), default = []): attach a CORS policy to the s3 bucket. default allows all.

### outputs
- bucket_id: id of the bucket that was created
- bucket_arn: arn of the bucket that was created
- bucket_regional_domain_name: regional domain name of the bucket

### example
```
module "kac_logs" {
  source = "../../../modules/aws/s3-bucket"

  bucket_name = "kac-logs"
  versioning  = false

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowALBAccessLogs",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::033677994240:root"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::kac-logs/*"
        },
        {
            "Sid": "AWSLogDeliveryWrite",
            "Effect": "Allow",
            "Principal": {
                "Service": "delivery.logs.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::kac-logs/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        },
        {
            "Sid": "AWSLogDeliveryAclCheck",
            "Effect": "Allow",
            "Principal": {
                "Service": "delivery.logs.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::kac-logs"
        }
    ]
}
EOF

  bucket_inventory         = true
  account_id               = local.account_id
  inventories = {
    "api_response"      = { inventory_enabled = true, inventory_prefix = "api_response" }
    "api_call"          = { inventory_enabled = true, inventory_prefix = "api_call" }
  }

  s3_bucket_notification = [
    {
      filter_prefix = "raw/lol"
      lambda_function_arn = module.lol_lambda.lambda_arn
      events        = ["s3:ObjectCreated:*"]
    },
    {
      filter_prefix = "raw/lol"
      filter_suffix = "kenny1"
      lambda_function_arn = data.terraform_remote_state.data.outputs.lol_lambda_arn
      events        = ["s3:ObjectCreated:*"]
    }
  ]

  cors_rule = [{
    allowed_headers = ["*"]
    allowed_methods = ["HEAD", "GET"]
    allowed_origins = ["*"]
  }]

  lifecycle_rule = [{
    id      = "delete-older-than-1"
    prefix  = "/beef"
    enabled = "true"
    tags    = {
      testing = "test-tag",
      lolkenny = "testing"
    }
    expiration = [{
      days = "1"
    }]
  },
    {
    id      = "delete-older-than-30"
    prefix  = "/chix"
    enabled = "true"
    tags    = {
      chicken = "test-tag",
      beef = "whats-for-dinner"
    }
    transition = [{
      days          = "30",
      storage_class = "STANDARD_IA"
      },
      {
        days          = "60"
        storage_class = "GLACIER"
    }],
    expiration = [{
      days = "90"
    }]
  }]
}
```


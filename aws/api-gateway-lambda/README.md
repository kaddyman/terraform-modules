# api-gateway-lambda

This module will create api-gateway that points to a lambda. 
This is the only approved way devops allows our lambdas to be exposed.

### inputs
- gateway_name (string): name of the api gateway
- path_part (string, default = "{proxy+}"): what uri paths to route to your endpoint.
- method_authorization (string, default = "NONE"): require authorization on the request to the api
- api_key_required (bool, default = true): require an api key on the request, should always be true
- method_parameters (string, default = null): extra parameters on the method 
- integration_type (string, default = "AWS"): integration type for the method
- integration_uri (string): lambda arn
- api_config (map): config options for stages and methods
- xray_tracing_enabled (bool, default = false): enable xray tracing for debugging
- data_trace_enabled (bool, default = false): enable data trace debugging
- throttling_rate_limit (int, default = 10000): set a rate limit for the method/stage
- throttling_burst_limit (int, default = 5000): set a burst limit for the method/stage
- api_description (string, default = null): add a description for your api gateway
- is_edge (bool, default = true): is this a edge or regional api gateway

### outputs
- api_gateway_stage_ids (list): list of the stage ids for your gateway
- api_gateway_id (string): id of your api gateway

### example
```
module "myip_gateway" {
  source = "../../../modules/aws/api-gateway-lambda"

  gateway_name         = "myip"
  updated_on           = "20200123-17:49:04"
  integration_uri      = module.myip_lambda.lambda_invoke_arn
  passthrough_behavior = "NEVER"
  request_templates = {
    "application/json" = <<EOT
#set($allParams = $input.params())
{
"body-json" : $input.json('$'),
"params" : {
#foreach($type in $allParams.keySet())
    #set($params = $allParams.get($type))
"$type" : {
    #foreach($paramName in $params.keySet())
    "$paramName" : "$util.escapeJavaScript($params.get($paramName))"
        #if($foreach.hasNext),#end
    #end
}
    #if($foreach.hasNext),#end
#end
},
"context" : {
    "api-id" : "$context.apiId",
    "api-key" : "$context.identity.apiKey",
    "http-method" : "$context.httpMethod",
    "stage" : "$context.stage",
    "source-ip" : "$context.identity.sourceIp",
    "user-agent" : "$context.identity.userAgent"
    }
}
EOT
  }

  api_config = {
    stages = {
      "prd" = "stage",
    }

    methods = {
      "POST" = "post_method"
    }

    method_settings = {
      prd-post = { stage = "prd", method = "POST" }
    }
  }
}
```

variable gateway_name {}
variable path_part { default = "{proxy+}" }
variable binary_media_types { default = null }
variable method_authorization { default = "NONE" }
variable api_key_required {
  type    = bool
  default = true
}
variable method_parameters { default = {} }
variable integration_type { default = "AWS" }
variable integration_uri { }
variable api_config {}
variable xray_tracing_enabled {
  type    = bool
  default = false
}
variable data_trace_enabled {
  type    = bool
  default = false
}
variable throttling_rate_limit { default = 10000 }
variable throttling_burst_limit { default = 5000 }
variable api_description { default = null }
variable is_edge {
  type    = bool
  default = true
}
variable updated_on {}
variable passthrough_behavior { default = "WHEN_NO_MATCH" }
variable content_handling { default = null }
variable request_templates { default = {} }

locals {
  default_tags = {
    terraform = "true"
  }
}

resource "aws_api_gateway_rest_api" "self" {
  name        = "${var.gateway_name}-private"
  description = var.api_description
  binary_media_types = var.binary_media_types

  endpoint_configuration {
    types = [var.is_edge == true ? "EDGE" : "REGIONAL"]
  }

  tags = local.default_tags
}

output "api_gateway_id" {
  value = aws_api_gateway_rest_api.self.id
}

output "api_gateway_execution_arn" {
  value = aws_api_gateway_rest_api.self.execution_arn
}

resource "aws_api_gateway_resource" "self" {
  rest_api_id = aws_api_gateway_rest_api.self.id
  parent_id   = aws_api_gateway_rest_api.self.root_resource_id
  path_part   = var.path_part
}

resource "aws_api_gateway_method" "self" {
  for_each = var.api_config.methods

  rest_api_id        = aws_api_gateway_rest_api.self.id
  resource_id        = aws_api_gateway_resource.self.id
  api_key_required   = var.api_key_required
  http_method        = each.key
  authorization      = var.method_authorization

  request_parameters = merge(var.method_parameters, {"method.request.path.proxy"=true,})
}

resource "aws_api_gateway_method_response" "self" {
  for_each = var.api_config.methods

  rest_api_id = aws_api_gateway_rest_api.self.id
  resource_id = aws_api_gateway_resource.self.id
  http_method = each.key
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }

  depends_on = [aws_api_gateway_method.self]
}

resource "aws_api_gateway_method_settings" "self" {
  for_each = var.api_config.method_settings

  rest_api_id = aws_api_gateway_rest_api.self.id
  stage_name  = each.value["stage"]
  method_path = each.value["method"] == "ANY" ? "*/*" : "${aws_api_gateway_resource.self.path_part}/${each.value["method"]}"

  settings {
    metrics_enabled        = true
    logging_level          = "INFO"
    data_trace_enabled     = var.data_trace_enabled
    caching_enabled        = false
    throttling_burst_limit = var.throttling_burst_limit
    throttling_rate_limit  = var.throttling_rate_limit
  }

  depends_on = [aws_api_gateway_method.self, aws_api_gateway_stage.self]
}

resource "aws_api_gateway_integration" "self" {
  for_each = var.api_config.methods

  rest_api_id             = aws_api_gateway_rest_api.self.id
  resource_id             = aws_api_gateway_resource.self.id
  http_method             = each.key
  integration_http_method = each.key
  content_handling        = var.content_handling
  passthrough_behavior    = var.passthrough_behavior
  request_templates       = var.request_templates

  type = var.integration_type
  uri  = var.integration_uri
  request_parameters = {
    "integration.request.path.proxy" = "method.request.path.proxy"
  }

  depends_on = [aws_api_gateway_method.self]
}

resource "aws_api_gateway_integration_response" "self" { 
  for_each = var.api_config.methods

  rest_api_id = aws_api_gateway_rest_api.self.id	
  resource_id = aws_api_gateway_resource.self.id	
  http_method = each.key
  status_code = "200"

  response_templates = {	
    "application/json" = ""	
  }	
}

resource "aws_api_gateway_stage" "self" {
  for_each = var.api_config.stages

  stage_name           = each.key
  rest_api_id          = aws_api_gateway_rest_api.self.id
  deployment_id        = aws_api_gateway_deployment.self[each.key].id
  xray_tracing_enabled = var.xray_tracing_enabled
  cache_cluster_enabled = false
}

output "api_gateway_stage_ids" {
  value = [
    sort(values(aws_api_gateway_stage.self)[*]["id"])
  ]
}

resource "aws_api_gateway_deployment" "self" {
  for_each = var.api_config.stages

  rest_api_id = aws_api_gateway_rest_api.self.id
  stage_name  = each.key
  description = "${var.updated_on} was the last terraform deployment"

  variables = {
    deployed_at = var.updated_on
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [aws_api_gateway_method.self, aws_api_gateway_integration.self]
}

output "api_gateway_deployment_stage_ids" {
  value = [
    sort(values(aws_api_gateway_deployment.self)[*]["stage_name"])
  ]
}


#output "deployment_stage_map" {
#  value = [
#    for stage_name, stage_id in zipmap(
#      sort(values(aws_api_gateway_deployment.self)[*]["stage_name"]),
#    sort(values(aws_api_gateway_stage.self)[*]["id"])) :
#    tomap({"stage_name" = stage_name, "stage_id" = stage_id})
#  ]
#}

# ecr

This will create a ecr 

Should only be called in global. 

### inputs
- ecr_name (string): name of the ecr
 
### outputs
- N/A

### example
```
module "ops_ecr" {
  source             = "../../../modules/aws/ecr"
  ecr_name = "ops"
}
```

variable ecr_name {}
variable enable_policy {
  default = false
  type = bool
}

locals {
  default_tags = {
    terralign   = "true"
  }
}

resource "aws_ecr_repository" "self" {
  name = var.ecr_name
  image_scanning_configuration {
    scan_on_push = true
  }
  tags = local.default_tags
}

resource "aws_ecr_repository_policy" "self" {
  count = var.enable_policy == true ? 1 : 0
  repository = aws_ecr_repository.self.name

  policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "ControlECRAccess",
      "Effect": "Allow",
      "Principal": {
        "AWS": ["arn:aws:iam::0000000000:root"]
      },
      "Action": [
        "ecr:BatchCheckLayerAvailability",
        "ecr:BatchGetImage",
        "ecr:GetDownloadUrlForLayer"
      ]
    }
  ]
}
EOF
}

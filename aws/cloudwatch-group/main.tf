variable env {default = "dev"}
variable group_name {}

locals {
  default_tags = {
    type        = "cloudwatch-group"
    owner       = "devops"
    terraform   = "true"
  }
}

resource "aws_cloudwatch_log_group" "self" {
  name              = var.group_name
  retention_in_days = var.env == "prd" ? 30 : 14

  tags = local.default_tags
}

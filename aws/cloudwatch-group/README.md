# cloudwatch group

This will create a cloudwatch group. If env is prd the retention is 30 days else it's 14 days.

### inputs
- env (string, default = "dev"): is this prd logs (rentention 30 days) otherwise retention is 14 days
- group_name (string): the name of the cloudwatch group
 
### outputs
- N/A

### example
```
module "ops_cloudwatch_group" {
  source             = "../../../../modules/aws/ecr"
  env        = "prd"
  group_name = "ops"
}
```

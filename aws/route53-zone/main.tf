variable hosted_zone {}

locals {
  default_tags = {
    terraform   = "true"
  }
}

resource "aws_route53_zone" "self" {
  name          = var.hosted_zone
  force_destroy = false

  tags = local.default_tags
}

output "zone_id" {
  value = aws_route53_zone.self.zone_id
}

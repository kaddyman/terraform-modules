# hosted-zone

This will create a new public hosted zone in your accounts route53.  

Should only be called in global. 

### inputs
- hosted_zone (string): hosted zone name
 
### outputs
- zone_id (string): hosted zone id, needed for route53 records

### example
```
module "addyman_io_zone" {
  source             = "../../../../modules/aws/route53-zone"
  hosted_zone        = "addyman.io"
}
```

# iam-mfa

This module creates forcemfa policy and password policy.

### inputs
- n/a

### outputs
- force_mfa_arn: arn for the force_mfa policy

### example
```
module "iam_mfa" {
  source         = "git::ssh://git@gitlab.com/terraform_modules.git//aws/iam_mfa?ref=main"
}
```

variable multi_az {
  default = false
  type = bool
}
variable multi_region {
  default = false
  type = bool
}
variable replication {
  default = false
  type = bool
}
variable access { default = "internal"}
variable environment { default = "not-defined" }
variable organization { default = "sagesure" }
variable team { default = "devops" }

locals {
  default_tags = {
    multi-az = var.multi_az
    multi-region = var.multi_region
    replication = var.replication
    access = var.access
    environment = var.environment
    organization = var.organization
    team = var.team
    service = "aws-asg"
    role = "not-defined"
    terralign = "true"
  }
}

data "aws_caller_identity" "current" {}

# password policy to be applied in every account
#
resource "aws_iam_account_password_policy" "strict" {
  minimum_password_length        = 15
  require_lowercase_characters   = true
  require_numbers                = true
  require_uppercase_characters   = true
  require_symbols                = true
  allow_users_to_change_password = true
  hard_expiry                    = false
  password_reuse_prevention      = 24
  max_password_age               = 90
}

# admin/mfa access policies
#
resource "aws_iam_policy" "force_mfa" {
  name        = "ForceMFA"
  description = "Improved version requiring MFA for console logins, limiting actions that a user can do w/o MFA (like set it up), but not limiting CLI"
  policy      = data.aws_iam_policy_document.force_mfa.json
}

data "aws_iam_policy_document" "force_mfa" {
  statement {
    sid    = "AllowAllUsersToListAccounts"
    effect = "Allow"

    actions = [
      "iam:ListAccountAliases",
      "iam:ListUsers",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/*",
    ]
  }

  statement {
    sid    = "AllowIndividualUserToTheirAccountInformation"
    effect = "Allow"

    actions = [
      "iam:GetAccountPasswordPolicy",
      "iam:GetAccountSummary",
      "iam:GetLoginProfile",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/$${aws:username}",
    ]
  }

  statement {
    sid    = "AllowIndividualUserToListTheirMFA"
    effect = "Allow"

    actions = [
      "iam:ListVirtualMFADevices",
      "iam:ListMFADevices",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:mfa/*",
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/$${aws:username}",
    ]
  }

  statement {
    sid    = "AllowIndividualUserToSeeAndManageOnlyTheirOwnAccountInformation"
    effect = "Allow"

    actions = [
      "iam:ChangePassword",
      "iam:CreateAccessKey",
      "iam:CreateLoginProfile",
      "iam:DeleteAccessKey",
      "iam:DeleteLoginProfile",
      "iam:GetLoginProfile",
      "iam:ListAccessKeys",
      "iam:UpdateAccessKey",
      "iam:UpdateLoginProfile",
      "iam:ListSigningCertificates",
      "iam:DeleteSigningCertificate",
      "iam:UpdateSigningCertificate",
      "iam:UploadSigningCertificate",
      "iam:ListSSHPublicKeys",
      "iam:GetSSHPublicKey",
      "iam:DeleteSSHPublicKey",
      "iam:UpdateSSHPublicKey",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/$${aws:username}",
    ]
  }

  statement {
    sid    = "AllowIndividualUserToManageThierMFA"
    effect = "Allow"

    actions = [
      "iam:CreateVirtualMFADevice",
      "iam:DeactivateMFADevice",
      "iam:DeleteVirtualMFADevice",
      "iam:EnableMFADevice",
      "iam:ResyncMFADevice",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:mfa/$${aws:username}",
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/$${aws:username}",
    ]
  }

  statement {
    sid    = "DoNotAllowAnythingOtherThanAboveUnlessMFAd"
    effect = "Deny"

    not_actions = ["iam:*"]
    resources   = ["*"]

    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"
      values   = ["false"]
    }
  }
}

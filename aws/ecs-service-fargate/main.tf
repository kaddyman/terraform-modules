variable service_name {}
variable container_port {}
variable ecs_cluster {}
variable desired_count { default = 1 }
variable account_id {}
variable env {}
variable region {}
variable vpc_id {}
variable policies { default = [] }
variable attach { default = null }
variable security_groups { default = [] }
variable groups {}
variable security_groups_egress { default = { rules = [{}] } }
variable cpu { default = 256 }
variable memory { default = 512 }
variable network_mode { default = "awsvpc" }
variable retention_in_days { default = 30 }
variable create_ecr_log { 
  default = true
  type    = bool 
}
variable assign_public_ip { 
  type = bool
  default = false
}
variable subnet_ids {}

locals {
  default_tags = {
    terralign   = "true"
  }
}

resource "aws_ecs_service" "self" {
  name                               = var.service_name
  cluster                            = var.ecs_cluster
  task_definition                    = aws_ecs_task_definition.self.arn
  desired_count                      = var.desired_count
  launch_type                        = "FARGATE"
  deployment_minimum_healthy_percent = 100

  network_configuration {
    subnets = var.subnet_ids
    security_groups = flatten([aws_security_group.self.*.id, var.security_groups])
    assign_public_ip = var.assign_public_ip
  }

  tags = local.default_tags
}

resource "aws_iam_role" "self" {
  name = "${var.service_name}-${var.env}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Action": "sts:AssumeRole",
          "Principal": {
              "Service": "ecs-tasks.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
      }
  ]
}
EOF

  tags = local.default_tags
}

data "aws_iam_policy_document" "self" {
  count = length(var.policies)
  dynamic "statement" {
    for_each = [for s in var.policies[count.index].statements : {
      effect     = lookup(s, "effect", "Allow")
      actions    = lookup(s, "actions", null)
      resources  = lookup(s, "resources", null)
      sid        = lookup(s, "sid", null)
      condition  = lookup(s, "condition", [])
    }]

    content {
      sid       = statement.value.sid
      effect    = statement.value.effect
      actions   = statement.value.actions
      resources = statement.value.resources

      dynamic "condition" {
        for_each = statement.value.condition

        content {
          test     = condition.value.test
          variable = condition.value.variable
          values   = condition.value.values
        }
      }
    }
  }
}

resource "aws_iam_policy" "self" {
  count  = length(var.policies)
  name   = "${var.service_name}-${var.policies[count.index].policy_name}-${var.env}"
  policy = data.aws_iam_policy_document.self[count.index].json
}

resource "aws_iam_role_policy_attachment" "self" {
  count      = length(var.policies)
  role       = aws_iam_role.self.name
  policy_arn = aws_iam_policy.self[count.index].arn
}

resource "aws_iam_role_policy_attachment" "attach" {
  count      = var.attach == null ? 0 : length(var.attach)
  role       = aws_iam_role.self.name
  policy_arn = var.attach[count.index]
}

resource "aws_ecs_task_definition" "self" {
  family        = var.service_name
  task_role_arn = aws_iam_role.self.arn
  execution_role_arn = "arn:aws:iam::${var.account_id}:role/ecsTaskExecutionRole"
  memory = var.memory
  cpu = var.cpu
  requires_compatibilities = ["FARGATE"]
  network_mode = var.network_mode
  container_definitions = <<DEFINITION
[
        {
            "dnsSearchDomains": null,
            "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-group": "${var.service_name}",
                    "awslogs-region": "${var.region}",
                    "awslogs-stream-prefix": "${var.env}"
                }
            },
            "entryPoint": null,
            "portMappings": [
                {
                    "hostPort": ${var.container_port},
                    "protocol": "tcp",
                    "containerPort": ${var.container_port}
                }
            ],
            "command": null,
            "linuxParameters": null,
            "environment": [
                {
                    "name": "ENV",
                    "value": "${var.env}"
                }
            ],
            "ulimits": null,
            "cpu": ${var.cpu},
            "memory": ${var.memory},
            "memoryReservation": ${var.memory},
            "dnsServers": null,
            "mountPoints": [],
            "workingDirectory": null,
            "dockerSecurityOptions": null,
            "volumesFrom": [],
            "image": "${var.account_id}.dkr.ecr.${var.region}.amazonaws.com/${var.service_name}:latest",
            "disableNetworking": null,
            "healthCheck": null,
            "essential": true,
            "links": null,
            "hostname": null,
            "extraHosts": null,
            "user": null,
            "readonlyRootFilesystem": null,
            "dockerLabels": null,
            "privileged": null,
            "name": "${var.service_name}"
        }
    ]
DEFINITION

  tags = local.default_tags
}

resource "aws_security_group" "self" {
  count  = length(var.groups)
  name   = var.groups[count.index].group_name
  vpc_id = var.vpc_id

  dynamic "egress" {
    for_each = [for r in var.security_groups_egress.rules : {
      from_port        = lookup(r, "from_port", 0)
      to_port          = lookup(r, "to_port", 0)
      protocol         = lookup(r, "protocol", "-1")
      cidr_blocks      = lookup(r, "cidr_blocks", ["0.0.0.0/0"])
      ipv6_cidr_blocks = lookup(r, "ipv6_cidr_blocks", null)
    }]

    content {
      from_port        = egress.value.from_port
      to_port          = egress.value.to_port
      protocol         = egress.value.protocol
      cidr_blocks      = egress.value.cidr_blocks
      ipv6_cidr_blocks = egress.value.ipv6_cidr_blocks
    }
  }

  dynamic "ingress" {
    for_each = [for r in var.groups[count.index].rules : {
      from_port        = lookup(r, "from_port", null)
      to_port          = lookup(r, "to_port", null)
      protocol         = lookup(r, "protocol", null)
      cidr_blocks      = lookup(r, "cidr_blocks", null)
      ipv6_cidr_blocks = lookup(r, "ipv6_cidr_blocks", null)
      security_groups  = lookup(r, "security_groups", null)
      self             = lookup(r, "self", false)
    }]

    content {
      from_port        = ingress.value.from_port
      to_port          = ingress.value.to_port
      protocol         = ingress.value.protocol
      cidr_blocks      = ingress.value.cidr_blocks
      ipv6_cidr_blocks = ingress.value.ipv6_cidr_blocks
      security_groups  = ingress.value.security_groups
      self             = ingress.value.self
    }
  }

  tags = merge(local.default_tags,
    tomap({
      "Name" = var.groups[count.index].group_name,
    })
  )
}

resource "aws_ecr_repository" "self" {
  count = var.create_ecr_log == true ?  1 : 0
  name  = var.service_name
  tags = local.default_tags
}

resource "aws_cloudwatch_log_group" "self" {
  count = var.create_ecr_log == true ? 1 : 0
  name              = var.service_name
  retention_in_days = var.retention_in_days

  tags = local.default_tags
}


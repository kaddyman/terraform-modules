# ecs-service-fargate

This will create a publics ervice in a ecs fargate cluster and all its 
dependencies. 

You should not use this to expose api's. I am using this to expose a 
single container that acts as a ssh socks5 proxy. everytime the 
container spins up it will change it's ip. It will open to the world 
so it will be vulnerable to D/DOS. Ideally you would use an API Gateway/ALB/
NLB/target group setup with fargate to secure the endpoints and ensure 
availability. not meant for PRODUCTION just POC and fun.

### inputs
- island (string): the island (account short name) that the resources will live in
- cluster (string): the cluster (vpc name) that the resources will live in
- lob (string): line of business this
- product (string): product this service belongs to
- owner (string): name of the owner of this bucket
- account_id (string): AWS account id of where this service will be running in
- service_name (string): name of the service, this will also be used for the name of the role, ecr, log group, target group and task definition family
- container_port (number): the port your service listens on
- ecs_cluster (string): ecs cluster arn of the cluster you want this service to run on
- desired_count (number, default=0): number of instances, you can always leave this at the default value as Jenkins deployments can control this
- env (string): environment variable for the task. Jenkins deployments can control this
- region (string): AWS region that the service runs in. used for ecr repo in task def. Jenkins deployments can control this
- vpc_id (string): vpc this service will run in 
- policies (list(object()), default = []): optional, what policies if any that should be attached to the service role. see example below
  - policy_name (string): name of the policy
  - statements (list(object())): actions and resources of the policy statement
- attach (list, default = null): optional, list of already existing policies to attach to the service. 

### outputs
- N/A

### example
```
module "ssh_tunnel" {
  source     = "../../../../modules/aws/ecs-service-fargate"
  account_id = local.account_id
  env        = "prd"
  region     = local.region

  service_name     = "ssh-tunnel"
  container_port   = 22
  ecs_cluster      = module.belial_ecs.ecs_cluster_arn
  desired_count    = 1
  cpu              = 256
  memory           = 512
  assign_public_ip = true

  security_groups = [module.network.sg_corporate_id]
  vpc_id          = module.network.vpc_id
  subnet_ids      = module.network.private_subnet_ids
  groups = [
    {
      group_name = "external_access_22",
      rules = [{
        from_port   = "22"
        to_port     = "22"
        protocol    = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
        },
        {
          from_port   = "22"
          to_port     = "22"
          protocol    = "TCP"
          cidr_blocks = ["0.0.0.0/0"]
      }]
    },
    {
      group_name = "external_access_all",
      rules = [{
        from_port   = 0
        protocol    = -1
        to_port     = 0
        cidr_blocks = ["10.66.0.0/16"]
        },
        {
          from_port   = 0
          protocol    = -1
          to_port     = 0
          cidr_blocks = ["10.66.0.0/16"]
      }]
    },
  ]
}
```

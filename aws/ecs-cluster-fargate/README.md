# ecs-cluster-fargate

This module creates a fargate ecs-cluster in a vpc.

### inputs
- name: (string) ecs cluster name
- cluster_type: (default = ecs) type of ecs cluster to spin up

### outputs
- ecs_cluster_id: ecs cluster id
- ecs_cluster_arn: ecs cluster arn

### example
```
module "belial_ecs" {
  source  = "../../../../modules/aws/ecs-cluster-fargate"
  name    = "belial-ecs"
}
```

variable name {}
variable cluster_type {
  default = "ecs"
}

locals {
  default_tags = {
    Name        = var.name
    terralign   = "true"
  }
}

resource "aws_ecs_cluster" "self" {
  name = var.name
  capacity_providers = ["FARGATE", "FARGATE_SPOT"]
  tags = local.default_tags
}

output "ecs_cluster_id" {
  value = aws_ecs_cluster.self.id
}

output "ecs_cluster_arn" {
  value = aws_ecs_cluster.self.arn
}


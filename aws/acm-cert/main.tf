variable alternative_names { default = null }
variable domain_name {}
variable certificate_authority_arn { default = null }

locals {
  default_tags = {
    terraform = "true"
  }
}

resource "aws_acm_certificate" "self" {
  domain_name               = var.domain_name
  validation_method         = var.certificate_authority_arn == null ? "DNS" : null
  subject_alternative_names = var.alternative_names
  certificate_authority_arn = var.certificate_authority_arn

  tags = merge(local.default_tags,
    tomap({
      "Name" = var.domain_name,
    })
  )

  lifecycle {
    create_before_destroy = true
  }
}

output "cert_arn" {
  value = aws_acm_certificate.self.arn
}

output "cert_id" {
  value = aws_acm_certificate.self.id
}

output "doamin_name" {
  value = aws_acm_certificate.self.domain_name
}


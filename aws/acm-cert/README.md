# acm-cert

This will create a acm certificate with DNS based validation and alternative 
names if needed.

Should only be called in global. the load balancers that will use these certs 
are VPC resources so the certs need to be global to be available to all VPCs.

### inputs
- domain_name (string): a domain name for which the certificate should be issued
- alternative_names (list, default=null ): optional, a list of domains that should be SANs in the issued certificate (ie `*.domain.com`)
- certificate_authority_arn (string, default = null): arn of the private root ca
 
### outputs
- cert_arn (string): arn of the acm certificate
- cert_id (string): id of the acm certificate
- domain_name (string): domain_name of the certificate

### example
```
module "addyman_io_cert" {
  source             = "../../../../modules/aws/acm-cert"
  domain_name        = "addyman.io"
  alternative_names  = ["*.addyman.io"]
}
```

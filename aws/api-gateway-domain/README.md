# api-gateway-doamin

This will setup a custom domain name for a api gateway. If you created a edge api 
gateway you need to create the same domain name type. If it is edge the acm cert 
must live in the us-east-1 region while regional types can have the cert live in 
the same region.

### inputs
- domain_name (string): fqdn for the new domain name
- acm_cert_arn (string): arn for the regional acm cert to use for the domain
- base_path_mappings (string): uri path to the api
- is_edge (bool, default = false): is this a edge or regional api gateway
 
### outputs
- N/A

### example
```
module "prd_api_belial_domain_name" {
  source = "../../../modules/aws/api-gateway-domain"

  domain_name  = "api.belial.io"
  acm_cert_arn = module.belial_io_cert.cert_arn
  is_edge      = true

  base_path_mappings = {
    "${module.myip_gateway.api_gateway_id}:prd:ip" = {
      api_id     = module.myip_gateway.api_gateway_id
      stage_name = "prd"
      base_path  = "ip"
    },
    "${module.ip2geo_gateway.api_gateway_id}:prd:geoip" = {
      api_id     = module.ip2geo_gateway.api_gateway_id
      stage_name = "prd"
      base_path  = "geoip"
    }
  }
}
```

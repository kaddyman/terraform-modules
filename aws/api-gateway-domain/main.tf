variable domain_name {}
variable is_edge {
  default = false
  type    = bool
}
variable acm_cert_arn {}
variable base_path_mappings {}

locals {
  default_tags = {
    terraform = "true"
  }
}

resource "aws_api_gateway_domain_name" "self" {
  domain_name              = var.domain_name
  regional_certificate_arn = var.is_edge == true ? null : var.acm_cert_arn
  certificate_arn          = var.is_edge == true ? var.acm_cert_arn : null
  security_policy          = "TLS_1_2"

  endpoint_configuration {
    types = [var.is_edge == true ? "EDGE" : "REGIONAL"]
  }

  tags = local.default_tags
}

output "domain_name" {
  value = var.is_edge == true ? aws_api_gateway_domain_name.self.cloudfront_domain_name : aws_api_gateway_domain_name.self.regional_domain_name
}

output "zone_id" {
  value = var.is_edge == true ? aws_api_gateway_domain_name.self.cloudfront_zone_id : aws_api_gateway_domain_name.self.regional_zone_id
}

resource "aws_api_gateway_base_path_mapping" "self" {
  for_each = var.base_path_mappings
  api_id      = each.value["api_id"]
  stage_name  = each.value["stage_name"]
  domain_name = aws_api_gateway_domain_name.self.domain_name
  base_path   = each.value["base_path"]
}

# iam-service-account

This module creates a iam group with users. This will also create policies to 
attach to the group. This is a service account only module and should NEVER 
be used for humans. NEVER create console logins for users created with this module.

### inputs
- island (string): the island (account short name) that the resources will live in
- cluster (string): the cluster (vpc name) that the resources will live in
- groupname (string) name of the iam group
- userlist (map) map made up of usernames as keys and values of tags for that user. users will also be added to the group.
- policies (list(map)): IAM access this service account has

### outputs
N/A

### example
```
module "testing_service_account" {
  source = "../../../modules/aws/iam-service-account"

  island  = local.island
  cluster = local.cluster

  groupname = "robots"

  userlist = {
    "robot1" = { email = "robot1@lol-robot.com" }
  }

  policies = [
    {
      policy_name = "sqs-access",
      statements = [{
        sid = "AllowSQSAccess",
        actions = [
          "sqs:*",
        ],
        resources = ["arn:aws:sqs:us-east-2:435676989:devops-queue.fifo"]
      }]
    },
    {
      policy_name = "cloudwatch-ro",
      statements = [{
        sid = "AllowCloudWatchRO",
        actions = [
          "autoscaling:Describe*",
          "cloudwatch:Describe*",
          "cloudwatch:Get*",
          "cloudwatch:List*",
          "ecr:GetAuthorizationToken",
          "logs:Get*",
          "logs:Describe*",
          "sns:Get*",
          "sns:List*"
        ],
        resources = ["*"]
      }]
    }
  ]
}
```

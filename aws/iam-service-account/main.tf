variable userlist {}
variable groupname {}
variable force_destroy {
  type    = bool
  default = true
}
variable policies {}

locals {
  default_tags = {
    terraform   = "true"
  }
}

resource "aws_iam_user" "self" {
  for_each      = var.userlist
  name          = each.key
  path          = "/"
  force_destroy = var.force_destroy

  tags = merge(local.default_tags,
    tomap({
      "team" = var.groupname,
      "email" = each.value["email"],
    })
  )
}

resource "aws_iam_group" "self" {
  name = var.groupname
  path = "/"
}

resource "aws_iam_group_membership" "self" {
  group = aws_iam_group.self.name
  name  = var.groupname
  users = [for k, v in var.userlist : k]
}

data "aws_iam_policy_document" "self_policy" {
  count = length(var.policies)
  dynamic "statement" {
    for_each = [for s in var.policies[count.index].statements : {
      effect     = lookup(s, "effect", "Allow")
      actions    = lookup(s, "actions", null)
      resources  = lookup(s, "resources", null)
      sid        = lookup(s, "sid", null)
      condition  = lookup(s, "condition", [])
    }]

    content {
      sid       = statement.value.sid
      effect    = statement.value.effect
      actions   = statement.value.actions
      resources = statement.value.resources

      dynamic "condition" {
        for_each = statement.value.condition

        content {
          test     = condition.value.test
          variable = condition.value.variable
          values   = condition.value.values
        }
      }
    }
  }
}

resource "aws_iam_policy" "self_policy" {
  count  = length(var.policies)
  name   = "${var.groupname}-${var.policies[count.index].policy_name}"
  policy = data.aws_iam_policy_document.self_policy[count.index].json
}

resource "aws_iam_group_policy_attachment" "self_policy" {
  count      = length(var.policies)
  group      = aws_iam_group.self.name
  policy_arn = aws_iam_policy.self_policy[count.index].arn
}

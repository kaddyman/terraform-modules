# aws-sftp

AWS sftp managed service. This will setup a aws managed sftp service with a s3 backend. 
You can have multiple users share the same home folder and each have their own login. 
You will need to manually create the home folder in the s3 bucket before users can login.


You will also have to set the custom domain for the sftp server manually since there isn't 
a terraform way to set that for some reason.

### inputs
- sftp_bucket_name (string): name of the s3 bucket to use for the sftp server storage
- user_list (map): map of users, home_folders and pub_keys to create.
 
### outputs
- N/A

### example
```
module "sftp_server" {
  source = "../../../modules/aws/aws-sftp"

  sftp_bucket_name = "abc-sftp-server"
  user_list = {
    "devops" = {
      "home_folder" = "devops"
      "pub_key"     = "ssh-rsa AAAAB3NzaC1yc2E-lol-x0vXYN"
    },
    "other-user" = {
      "home_folder" = "other-home"
      "pub_key"     = "ssh-rsa AAAAB-lol-U0ou1FP8KuTp3xO5L"
    }
  }
}
```

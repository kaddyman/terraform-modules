variable user_list {}
variable sftp_bucket_name {}

locals {
  default_tags = {
    terraform   = "true"
  }
}

module "s3_bucket" {
  source      = "../s3-bucket"
  island      = var.island
  cluster     = var.cluster
  lob         = var.lob
  product     = "sftp"
  owner       = "devops"
  bucket_name = var.sftp_bucket_name
  versioning  = false

  lifecycle_rule = [{
    id      = "delete-older-than-30-days"
    enabled = "true"
    expiration = [{
      days = "30"
    }]
  }]
}

resource "aws_transfer_server" "self" {
  identity_provider_type   = "SERVICE_MANAGED"
  logging_role             = aws_iam_role.self.arn

  tags = local.default_tags
}

resource "aws_transfer_user" "self" {
  for_each                 = var.user_list

  server_id                = aws_transfer_server.self.id
  user_name                = each.key
  role                     = aws_iam_role.self_user[each.key].arn
  home_directory           = "/${module.s3_bucket.bucket_id}/${each.value["home_folder"]}/"

  tags = local.default_tags
}

resource "aws_transfer_ssh_key" "self" {
  for_each      = var.user_list

  server_id     = aws_transfer_server.self.id
  user_name     = each.key
  body          = each.value["pub_key"]
  depends_on    = [aws_transfer_user.self]
}

# sftp logging role

resource "aws_iam_role" "self" {
  name               = "sftp-${var.sftp_bucket_name}"
  assume_role_policy = data.aws_iam_policy_document.self_assume.json

  tags = local.default_tags
}

data "aws_iam_policy_document" "self_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["transfer.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "self" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:GetLogEvents",
      "logs:FilterLogEvents"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "self" {
  name   = "transfer-server-role-${var.sftp_bucket_name}"
  policy = data.aws_iam_policy_document.self.json
}

resource "aws_iam_role_policy_attachment" "self" {
  role       = aws_iam_role.self.name
  policy_arn = aws_iam_policy.self.arn
}

# sftp user roles

resource "aws_iam_role" "self_user" {
  for_each = var.user_list

  name               = "sftp-${var.sftp_bucket_name}-${each.key}"
  assume_role_policy = data.aws_iam_policy_document.self_assume.json

  tags = local.default_tags
}

data "aws_iam_policy_document" "self_user" {
  for_each = var.user_list

  statement {
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      "arn:aws:s3:::${module.s3_bucket.bucket_id}"
    ]
  }

  statement {
    actions = [
      "s3:*Object",
      "s3:DeleteObjectVersion",
      "s3:GetObjectVersion",
      "s3:GetObjectACL",
      "s3:PutObjectACL"
    ]
    resources = [
      "arn:aws:s3:::${module.s3_bucket.bucket_id}/${each.value["home_folder"]}/*"
    ]
  }
}

resource "aws_iam_policy" "self_user" {
  for_each = var.user_list

  name   = "transfer-server-role-${var.sftp_bucket_name}-${each.key}"
  policy = data.aws_iam_policy_document.self_user[each.key].json
}

resource "aws_iam_role_policy_attachment" "self_user" {
  for_each = var.user_list

  role       = aws_iam_role.self_user[each.key].name
  policy_arn = aws_iam_policy.self_user[each.key].arn
}

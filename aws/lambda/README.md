# lambda

This module can create a lambda with various triggers. It can also setup lambdas that run in VPCs.

### inputs
- island (string): the island (account short name) that the resources will live in
- cluster (string): the cluster (vpc name) that the resources will live in
- lob (string): line of business this resource belongs to
- product (string): product this resource belongs to
- owner (string): name of the owner of this resource
- region (string): region that the lambda will run in
- account_id (string): account id that the lambda will run in
- env (string): the three letter code for the env this lambda runs in
- reserved_concurrent_executions (int, default = null): number of concurrent executions, default is no limit
- handler (string, default = "lambda_function.lambda_handler"): function name that the lambda runs when executed 
- runtime (string, default = "python3.7"): code execution lanuage that lambda code uses
- memory_size (int, default = 256): amount of memory the lambda needs to execute
- timeout (int, default = 30): time in seconds until lambda will exit
- lambda_bucket_name (string): bucket that holds your lambda code zip file
- lambda_env_vars (map, default = {}): map of any additional env vars you need to set for the lambda
- policies (map, default = {}): additional access this lambda will need, s3 buckets, etc
- security_groups (list, default = []): list of security group ids to attached this lambda, only needed if running in a VPC
- rules (map, default = {}): custom security groups rules for ingress, only needed if running in a VPC
- vpc_id (string, default = null): vpc for the lambda to run in, only needed if running in a VPC
- subnet_ids (string, default = null): subnet ids that this lambda will run in
- identifiers (list, default = []): list of additional service names to attach to the lambda role, needed if your lambda starts up ecs tasks in a cluster
- attach (list, default = null): list of existing aws policies to attach to the lambda role
- execution_type (string, default = null): what triggers runs of this lambda. ie s3, sns or events
- lambda_permission (map, default = {}): granting access to s3/sns/events to launch this lambda
- topic_arn (string, default = null): sns topic arn
- schedule_expression (string, default = null): ecs cron/rate for the cloudwatch event rule
 
### outputs
- lambda_arn (string): The Amazon Resource Name (ARN) identifying your Lambda Function
- lambda_qualified_arn (string): The Amazon Resource Name (ARN) identifying your Lambda Function Version (if versioning is enabled via publish = true) string
- lambda_invoke_arn (string): The ARN to be used for invoking Lambda Function from API Gateway - to be used in aws_api_gateway_integration's uri

### example

Running a lmabda in a VPC w/ s3 bucket access.
```
module "testing_lambda" {
  source     = "../../../../modules/aws/lambda"
  island     = local.island
  cluster    = local.cluster
  product    = "testing"
  owner      = "devops"
  lob        = "kenny"
  region     = local.region
  account_id = local.account_id
  env        = "dev"

  lambda_name        = "testing"
  lambda_bucket_name = data.terraform_remote_state.realm.outputs.kac_lambdas_id
  timeout            = 30
  memory_size        = 256

  reserved_concurrent_executions = 5

  security_groups = [module.network.sg_corporate_id]
  vpc_id          = module.network.vpc_id
  subnet_ids      = module.network.data_subnet_ids

  policies = [
    {
      policy_name = "s3-access",
      statements = [{
        actions   = [
          "s3:ListBucket",
          "s3:GetObject",
          "s3:PutObject"
        ],
        resources = [
          "arn:aws:s3:::kac-lol",
          "arn:aws:s3:::kac-lol/*",
        ]
      }],
    },
    {
      policy_name = "network-access",
      statements = [{
        actions = [
          "ec2:DescribeNetworkInterfaces",
          "ec2:CreateNetworkInterface",
          "ec2:DeleteNetworkInterface",
          "ec2:DescribeInstances",
          "ec2:AttachNetworkInterface"
        ],
        resources = ["*"]
      }],
    }
  ]

  # Supported execution types s3, events, sns
  execution_type = "s3"
  lambda_permission = {
    kac-configs = "arn:aws:s3:::kac-configs"
  }
}

output "testing_lambda_lambda_arn" {
  vaule = module.testing_lambda.lambda_arn
}
```

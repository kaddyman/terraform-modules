variable region {}
variable account_id {}
variable env {}
variable lambda_name {}
variable reserved_concurrent_executions { default = null }
variable handler { default = "lambda_function.lambda_handler" }
variable runtime { default = "python3.7" }
variable memory_size { default = 256 }
variable timeout { default = 30 }
variable lambda_bucket_name {}
variable lambda_env_vars { default = {} }
variable policies { default = [] }
variable security_groups { default = [] }
variable rules { default = {} }
variable vpc_id { default = null }
variable subnet_ids { default = null }
variable identifiers { default = [] }
variable attach { default = null }
variable execution_type { default = null }
variable lambda_permission { default = {} }
variable topic_arn { default = null }
variable schedule_expression { default = null }

locals {
  default_tags = {
    terralign = "true"
  }
}

resource "aws_lambda_function" "self" {
  s3_bucket     = var.lambda_bucket_name
  s3_key        = "${var.lambda_name}/${var.lambda_name}.zip"
  function_name = var.lambda_name
  handler       = var.handler
  role          = aws_iam_role.self.arn
  runtime       = var.runtime
  memory_size   = var.memory_size
  timeout       = var.timeout
  reserved_concurrent_executions = var.reserved_concurrent_executions

  environment {
    variables = merge(var.lambda_env_vars,
    tomap({
        "REGION" = var.region,
        "ENV" = var.env
      })
    )
  }

  dynamic "vpc_config" {
    for_each = var.vpc_id == null ? [] : [1]
    content {
      subnet_ids         = var.subnet_ids
      security_group_ids = flatten([aws_security_group.self[0].id, var.security_groups])
    }
  }

  lifecycle {
    ignore_changes = [layers]
  }

  tags = local.default_tags
}

output "lambda_arn" {
  value = aws_lambda_function.self.arn
}

output "lambda_qualified_arn" {
  value = aws_lambda_function.self.qualified_arn
}

output "lambda_invoke_arn" {
  value = aws_lambda_function.self.invoke_arn
}

resource "aws_lambda_permission" "self" {
  for_each = var.lambda_permission

  statement_id  = "AllowExecutionFrom${var.execution_type}"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.self.id
  principal     = "${var.execution_type}.amazonaws.com"
  source_arn    = var.execution_type == "events" ? aws_cloudwatch_event_rule.self[0].arn : each.value
}

resource "aws_sns_topic_subscription" "self" {
  count = var.execution_type == "sns" ? 1 : 0
  topic_arn              = var.topic_arn
  protocol               = "lambda"
  endpoint               = aws_lambda_function.self.arn
  endpoint_auto_confirms = true
}

resource "aws_cloudwatch_event_rule" "self" {
  count = var.execution_type == "events" ? 1 : 0
  name                = "${var.lambda_name}-${var.env}"
  schedule_expression = var.schedule_expression
}

resource "aws_cloudwatch_event_target" "self" {
  count = var.execution_type == "events" ? 1 : 0
  target_id = "${var.lambda_name}-${var.env}"
  arn       = aws_lambda_function.self.arn
  rule      = aws_cloudwatch_event_rule.self[0].name
}

data "aws_iam_policy_document" "role_policy" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]
    principals {
      type = "Service"
      identifiers = flatten(["lambda.amazonaws.com", var.identifiers])
    }
  }
}

resource "aws_iam_role" "self" {
  name        = "${var.lambda_name}-role"
  assume_role_policy = data.aws_iam_policy_document.role_policy.json

  tags = local.default_tags
}

data "aws_iam_policy_document" "cloudwatach_logs" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]
    resources = ["arn:aws:logs:*:*:*"]
  }
}

resource "aws_iam_policy" "cloudwatach_logs" {
  name   = "${var.lambda_name}-logs"
  policy = data.aws_iam_policy_document.cloudwatach_logs.json
}

resource "aws_iam_role_policy_attachment" "cloudwatach_logs" {
  role       = aws_iam_role.self.name
  policy_arn = aws_iam_policy.cloudwatach_logs.arn
}

data "aws_iam_policy_document" "locker" {
  statement {
    actions = [
      "dynamodb:Describe*",
      "dynamodb:Get*",
      "dynamodb:List*",
      "dynamodb:Query",
      "dynamodb:Scan"
    ]
    resources = [
      "arn:aws:dynamodb:${var.region}:${var.account_id}:table/lambda_locks",
      "arn:aws:dynamodb:${var.region}:${var.account_id}:table/lambda_locks/index/*"
    ]
  }
}

resource "aws_iam_policy" "locker" {
  name   = "${var.lambda_name}-locker"
  policy = data.aws_iam_policy_document.locker.json
}

resource "aws_iam_role_policy_attachment" "locker" {
  role       = aws_iam_role.self.name
  policy_arn = aws_iam_policy.locker.arn
}

data "aws_iam_policy_document" "self" {
  count = length(var.policies)
  dynamic "statement" {
    for_each = [for s in var.policies[count.index].statements : {
      effect     = lookup(s, "effect", "Allow")
      actions    = lookup(s, "actions", null)
      resources  = lookup(s, "resources", null)
      sid        = lookup(s, "sid", null)
      condition  = lookup(s, "condition", [])
    }]

    content {
      sid       = statement.value.sid
      effect    = statement.value.effect
      actions   = statement.value.actions
      resources = statement.value.resources

      dynamic "condition" {
        for_each = statement.value.condition

        content {
          test     = condition.value.test
          variable = condition.value.variable
          values   = condition.value.values
        }
      }
    }
  }
}

resource "aws_iam_policy" "self" {
  count  = length(var.policies)
  name   = "${var.lambda_name}-${var.policies[count.index].policy_name}"
  policy = data.aws_iam_policy_document.self[count.index].json
}

resource "aws_iam_role_policy_attachment" "self" {
  count      = length(var.policies)
  role       = aws_iam_role.self.name
  policy_arn = aws_iam_policy.self[count.index].arn
}

resource "aws_iam_role_policy_attachment" "attach" {
  count      = var.attach == null ? 0 : length(var.attach)
  role       = aws_iam_role.self.name
  policy_arn = var.attach[count.index]
}

resource "aws_security_group" "self" {
  count  = var.vpc_id == null ? 0 : 1
  name   = var.lambda_name
  vpc_id = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  dynamic "ingress" {
    for_each = var.rules 

    content {
      from_port        = ingress.value.from_port
      to_port          = ingress.value.to_port
      protocol         = ingress.value.protocol
      cidr_blocks      = ingress.value.cidr_blocks
    }
  }

  tags = local.default_tags
}

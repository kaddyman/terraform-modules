variable usage_plan_name {}
variable api_stages {}
variable api_key_config {}

locals {
  default_tags = {
    terraform = "true"
  }
}

resource "aws_api_gateway_api_key" "self" {
  for_each = var.api_key_config

  name    = each.key
  enabled = each.value["enabled"]

  tags = local.default_tags
}

resource "aws_api_gateway_usage_plan" "self" {
  name = var.usage_plan_name

  dynamic "api_stages" {
    for_each = [for r in var.api_stages : {
      api_id = lookup(r, "api_id", null)
      stage  = lookup(r, "stage", null)
    }]

    content {
      api_id = api_stages.value.api_id
      stage  = api_stages.value.stage
    }
  }

  depends_on = [aws_api_gateway_api_key.self]
}

resource "aws_api_gateway_usage_plan_key" "self" {
  for_each = var.api_key_config

  key_id        = aws_api_gateway_api_key.self[each.key].id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.self.id

  depends_on = [aws_api_gateway_api_key.self, aws_api_gateway_usage_plan.self]
}

# api-gateway-usage-plan

This will create x api-keys and one usage plan.

### inputs
- api_key_name (string): name of the api key to create
- usage_plan_name (string): name of the usage plan
- api_stages (map): which apis and stages to attach to this usage plan
- create_api_key (bool, default = true): create an api-key and usage plan otherwise only create an usage plan
- api_key_id (string, default = null): attach an already existing api key to a usage plan. create_api_key must be false to use this option.
 
### outputs
N/A

### example
```
module "belial_usage_prd" {
  source = "../../../modules/aws/api-gateway-usage-key"

  api_key_config = {
    "belial" = { enabled = true }
  }

  usage_plan_name = "belial"

  api_stages = [
    {
      api_id = module.myip_gateway.api_gateway_id,
      stage  = "prd"
    },
    {
      api_id = module.ip2geo_gateway.api_gateway_id,
      stage  = "prd"
    }
  ]
}
```

# ses-domain

This module will add domains to us-east-1 ses so we can send emails to them.


This module should be called from global since it's region dependant not vpc. 
Should also note due to the limits of ses region availability we don't create 
a new region in terralign for this and just use a alias provider for it.


If you want to import domains already in aws you will need to add a new provider
with a alias of `ses` and region of `us-east-1`. When you run terraform use the
`-provider=aws.ses` option on the import or it will fail with region errors.

### inputs
- account_id (string): AWS account id of where you are adding emails
- ses_region (string, default=us-east-1): ses region we default to us-east-1
- domain (string): domain to enable for ses
- dkim_enabled (bool, default=false): enable/disable dkim for a domain

### outputs
N/A

### example
```
module "ses_domain_addyman_io" {
  source = "../../../modules/aws/ses-domain"

  account_id   = local.account_id
  ses_region   = "us-east-1"
  domain       = "addyman.io"
  dkim_enabled = false
}
```

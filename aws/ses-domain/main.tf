variable ses_region { default = "us-east-1" }
variable account_id {}
variable domain {}
variable dkim_enabled {
  type    = bool
  default = false
}

provider "aws" {
  alias  = "ses"
  region = var.ses_region

  assume_role {
    role_arn     = "arn:aws:iam::${var.account_id}:role/terralign" # change account number to target
    session_name = "terralign"
    external_id  = "terralign_id"
  }
}

resource "aws_ses_domain_identity" "self" {
  provider = aws.ses
  domain   = var.domain
}

resource "aws_ses_domain_dkim" "self" {
  provider = aws.ses
  count    = var.dkim_enabled == true ? 1 : 0
  domain   = aws_ses_domain_identity.self.domain
}

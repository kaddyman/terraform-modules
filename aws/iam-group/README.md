# iam-group

This module creates a iam group and users. it will automatically attach 
ForceMFA, ReadOnly or AdminAccess based on options passed to it. You will
still have to setup console access and passwords manually after user creation. 


This module will ONLY be used in control except for the 1 doomsday user we will 
leave in each account. If you need to setup role/policies for these groups to 
assume into use the iam-role module. Path is always `/` otherwise it will 
conflict with our ForceMFA policy.

### inputs
- account_id (string): account id of the target, used for finding the force_mfa policy
- groupname (string) name of the iam group
- userlist (map) map made up of usernames as keys and values of tags for that user. users will also be added to the group.
- assumeroles (list) the list of role arns that this group can assume into
- give_fulladmin (bool, default=false) this will give full admin access if true
- give_readonly (bool, default=false) this will give full read only access if true
- force_destroy (bool, default=true): delete this user even if member of group or attached policies
- is_doom (bool, default=false): only used for the doomsdays group/devops-admin user that has to b e in every account in the event something goes wrong

### outputs
N/A

### example
```
module "devops_admins" {
  source = "../../../modules/aws/iam-group"
  account_id = local.account_id

  groupname      = "devops-admins"
  give_fulladmin = true

  userlist = {
    "ken.addyman" = { email = "ken@addyman.io" }
  }

  assumeroles = [
    "arn:aws:iam::09745872408:role/devops-admin",
  ]
}
```

variable account_id {}
variable userlist {}
variable groupname {}
variable assumeroles { default = [] }
variable is_doom {
  type = bool
  default = false
}
variable give_fulladmin {
  type    = bool
  default = false
}
variable give_readonly {
  type    = bool
  default = false
}
variable force_destroy {
  type    = bool
  default = true
}

locals {
  default_tags = {
    terraform   = "true"
  }
}

resource "aws_iam_user" "self" {
  for_each      = var.userlist
  name          = each.key
  path          = "/"
  force_destroy = var.force_destroy

  tags = merge(local.default_tags,
    tomap({
      "team" = var.groupname,
      "email" = each.value["email"],
    })
  )
}

resource "aws_iam_group" "self" {
  name = var.groupname
  path = "/"
}

resource "aws_iam_group_membership" "self" {
  group = aws_iam_group.self.name
  name  = var.groupname
  users = [for k, v in var.userlist : k]
}

resource "aws_iam_group_policy_attachment" "force_mfa" {
  group      = aws_iam_group.self.name
  policy_arn = "arn:aws:iam::${var.account_id}:policy/ForceMFA"
}

resource "aws_iam_group_policy_attachment" "give_fulladmin" {
  count      = var.give_fulladmin ? 1 : 0
  group      = aws_iam_group.self.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_group_policy_attachment" "give_readonly" {
  count      = var.give_readonly ? 1 : 0
  group      = aws_iam_group.self.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

data "aws_iam_policy_document" "assumeroles" {
  count = var.is_doom ? 0 : 1
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    resources = var.assumeroles
  }
}

resource "aws_iam_policy" "assumeroles" {
  count = var.is_doom ? 0 : 1
  name   = "${var.groupname}-assume-roles"
  policy = data.aws_iam_policy_document.assumeroles[0].json
}

resource "aws_iam_group_policy_attachment" "assumeroles" {
  count = var.is_doom ? 0 : 1
  group      = aws_iam_group.self.name
  policy_arn = aws_iam_policy.assumeroles[0].arn
}

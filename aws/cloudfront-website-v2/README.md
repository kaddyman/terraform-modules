# cloudfront-website

This will create a cloudfront distribution for a s3 website. The bucket can be private
and does not need to have static web hosting enabled. This allows us to have a private
s3 bucket host the website and the only access point being cloudfront.

Be sure to point your route53 dns records for foo.io A record alias to your cloudfront distribution.
Add add a CNAME record for www.foo.io to point to foo.io

### inputs
- bucket_name (string): name of the bucket that holds the website files
- versioning (bool default = true): enable object versioning in s3 bucket
- policy (string EOF, default = null): custom policy for s3 bucket, default will create policy that allow cloudfont access to bucket objects
- logging (map, default = null): enable object logging, disabled by default
- s3_cors_rule (list, default = []): CORS rules you wish your bucket to support, disabled by default

- aliases (list, default = null): list of aliases for the cloudfront distribution
- log_bucket_name (string): which bucket to send the access logs to
- enabled (bool, default = true): enable/disable this cloudfront distribution
- acm_certificate_arn (string): arn of the acm cert to use for this distribution (must be in us-east-1)
- comment (stringi, default = "Cloudfront origin access identity comment"): cloudfront origin access identity name
- minimum_protocol_version (string, default = "TLSv1.2_2021"): which ssl protocol to use
- lambda_function_association (list(map), default = []): list of lambdas@edge to attach to this cloudfront website for pre/post porcessing

### outputs
- domain_name (string): cloudfront domain name
- hosted_zone_id (string): cloudfront hosted zone id
- distro_id (string): cloudfront distro id
- bucket_id (string): s3 bucket name
- bucket_arn (string): s3 bucket arn
- bucket_regional_domain_name (string): regional s3 bucket domain name

### examples

# cors rule example
```
  cors_rule = [{
    allowed_headers = ["*"]
    allowed_methods = ["HEAD", "GET"]
    allowed_origins = ["*"]
  }]
```

# module example
```
module "reaper_website" {
  source              = "git::ssh://git@gitlab.net/kaddyman/terraform-modules.git//aws/cloudfront_website?ref=cloudfront-s3-module"
  bucket_name         = "reaper-website-testing"
  aliases             = ["reaper-test.example.com"]
  log_bucket_name     = "example-cloudwatch-logs"
  acm_certificate_arn = module.acm_example.arn
}
```

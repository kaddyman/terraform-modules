# S3 variables
variable bucket_name {}
variable versioning {
  type    = bool
  default = true
}
variable policy {
  type    = string
  default = null
}
variable logging {
  type = object({
    target_bucket = string
    target_prefix = string
  })
  default = null
}
variable s3_cors_rule { 
  default = [] 
}
variable origins { default = [] }

# ---

# cloudfront variables
variable aliases { default = null }
variable log_bucket_name {}
variable enabled {
  type    = bool
  default = true
}
variable acm_certificate_arn {}
variable comment { default = "Cloudfront origin access identity comment" }
variable minimum_protocol_version { default = "TLSv1.2_2021"}
variable lambda_function_association { default = [] }

# ---

# module tags
variable multi_az {
  default = false
  type = bool
}
variable multi_region {
  default = true
  type = bool
}
variable replication {
  default = false
  type = bool
}
variable access { default = "external"}
variable environment { default = "not-defined" }
variable organization { default = "kac" }
variable team { default = "devops" }

locals {
  default_tags = {
    multi-az = var.multi_az
    multi-region = var.multi_region
    replication = var.replication
    access = var.access
    environment = var.environment
    organization = var.organization
    team = var.team
    service = "cloudfront-s3-website"
    role = "not-defined"
    terralign = "true"
  }
}

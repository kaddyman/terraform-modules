data "aws_iam_policy_document" "self" {
  statement {
    sid = "CloudFrontS3Policy"
    actions = [
      "s3:GetObject"
    ]
    resources = [
      "arn:aws:s3:::${var.bucket_name}/*"
    ]
    principals {
      type = "AWS"
      identifiers = [
        aws_cloudfront_origin_access_identity.self.iam_arn
      ]
    }
  }
}

resource "aws_s3_bucket" "self" {
  bucket        = var.bucket_name
  acl           = "private"
  force_destroy = false
  policy        = var.policy == null ? data.aws_iam_policy_document.self.json : var.policy

  versioning {
    enabled = true
  }

  dynamic "logging" {
    for_each = var.logging == null ? [] : [1]
    content {
      target_bucket = var.logging.target_bucket
      target_prefix = var.logging.target_prefix
    }
  }

  dynamic "cors_rule" {
    for_each = [for c in var.s3_cors_rule : {
      allowed_headers = lookup(c, "allowed_headers", ["*"])
      allowed_methods = lookup(c, "allowed_methods", ["HEAD", "GET"])
      allowed_origins = lookup(c, "allowed_origins", ["*"])
      max_age_seconds = lookup(c, "max_age_seconds", 3000)
    } if c != length(var.s3_cors_rule)]

     content {
       allowed_headers = cors_rule.value.allowed_headers
       allowed_methods = cors_rule.value.allowed_methods
       allowed_origins = cors_rule.value.allowed_origins
       max_age_seconds = cors_rule.value.max_age_seconds
     }
  }

  tags = local.default_tags

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "self" {
  bucket = aws_s3_bucket.self.id
  ignore_public_acls      = true
  restrict_public_buckets = true
  block_public_acls       = true
  block_public_policy     = true
}

output "bucket_id" {
  value = aws_s3_bucket.self.id
}

output "bucket_arn" {
  value = aws_s3_bucket.self.arn
}

output "bucket_regional_domain_name" {
  value = aws_s3_bucket.self.bucket_regional_domain_name
}

variable gateway_name {}
variable disable_execute_api_endpoint {
  type    = bool
  default = true
}
variable routes {}
variable integration_uri {}
variable stage_name { default = "$default" }

locals {
  default_tags = {
    terralign = "true"
  }
}

resource "aws_apigatewayv2_api" "self" {
  name        = var.gateway_name
  protocol_type = "HTTP"
  disable_execute_api_endpoint = var.disable_execute_api_endpoint

  tags = local.default_tags
}

output "api_gateway_id" {
  value = aws_apigatewayv2_api.self.id
}

output "api_gateway_execution_arn" {
  value = aws_apigatewayv2_api.self.execution_arn
}

resource "aws_apigatewayv2_stage" "self" {
  api_id = aws_apigatewayv2_api.self.id
  name   = var.stage_name
  auto_deploy = true

  default_route_settings {
    throttling_burst_limit = 100
    throttling_rate_limit  = 100
  }
}

resource "aws_apigatewayv2_route" "self" {
  for_each  = var.routes
  api_id    = aws_apigatewayv2_api.self.id
  route_key = each.value
  target    = "integrations/${aws_apigatewayv2_integration.self.id}"
}

resource "aws_apigatewayv2_integration" "self" {
  api_id                    = aws_apigatewayv2_api.self.id
  integration_type          = "AWS_PROXY"
  connection_type           = "INTERNET"
  integration_method        = "POST"
  integration_uri           = var.integration_uri
  passthrough_behavior      = "WHEN_NO_MATCH"
  payload_format_version    = "2.0"
}

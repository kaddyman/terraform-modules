# api-gatewayv2-lambda

This module will create an http api-gateway that points to a lambda. 

### inputs
- gateway_name (string): name of the api gateway
- integration_uri (string): lambda arn
- routes (map): map of the routes/methods allowed on this gateway

### outputs
- api_gateway_id (string): id of the api gateway
- api_gateway_execution_arn (string): execution arn of the gateway

### example
```
module "locks_gatewayv2" {
  source          = "../../../modules/aws/api-gatewayv2-lambda"
  gateway_name    = "locks-v3"
  integration_uri = module.locks_lambda.lambda_invoke_arn
  routes = {
    get-helloworld = "GET /helloworld"
    get-foo        = "GET /foo"
    delete-foo     = "DELETE /foo"
    post-foo       = "POST /foo"
    get-bar        = "GET /bar"
    delete-bar     = "DELETE /bar"
    post-bar       = "POST /bar"
    post-remove    = "POST /remove"
    post-add       = "POST /add"
    post-status    = "POST /status"
  }
}
```


variable aliases { default = null }
variable bucket_name {}
variable bucket_regional_domain_name {}
variable log_bucket_name {}
variable enabled {
  type    = bool
  default = true
}
variable acm_certificate_arn {}
variable comment {}
variable minimum_protocol_version { default = "TLSv1.2_2021"}
variable lambda_function_association { default = [] }

locals {
  default_tags = {
    terralign = "true"
  }
}

resource "aws_cloudfront_origin_access_identity" "self" {
  comment = var.comment
}

resource "aws_cloudfront_distribution" "self" {
  origin {
    domain_name = var.bucket_regional_domain_name
    origin_id   = "S3-${var.bucket_name}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.self.cloudfront_access_identity_path
    }
  }

  enabled             = var.enabled
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  logging_config {
    include_cookies = false
    bucket          = "${var.log_bucket_name}.s3.amazonaws.com"
    prefix          = var.bucket_name
  }

  aliases = var.aliases

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "S3-${var.bucket_name}"

    forwarded_values {
      query_string = false
      headers      = [
       "Access-Control-Request-Headers",
       "Access-Control-Request-Method",
       "Origin"
     ]

      cookies {
        forward = "none"
      }
    }

    dynamic "lambda_function_association" {
      for_each = [for r in var.lambda_function_association : {
        event_type   = lookup(r, "event_type", null)
        include_body = lookup(r, "include_body", null)
        lambda_arn   = lookup(r, "lambda_arn", null)
      } if r != length(var.lambda_function_association)]
    
      content {
        event_type    = lambda_function_association.value.event_type
        include_body  = lambda_function_association.value.include_body
        lambda_arn    = lambda_function_association.value.lambda_arn
      }
    }

  viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    compress               = true
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = var.acm_certificate_arn
    ssl_support_method  = "sni-only"
    minimum_protocol_version = var.minimum_protocol_version
  }

  tags = local.default_tags
}

output "domain_name" {
  value = aws_cloudfront_distribution.self.domain_name 
}

output "hosted_zone_id" {
  value = aws_cloudfront_distribution.self.hosted_zone_id
}

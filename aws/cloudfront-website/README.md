# cloudfront-website

This will create a cloudfront distribution for a s3 website. The bucket can be private 
and does not need to have static web hosting enabled. This allows us to have a private 
s3 buckets host the website and the only access point being cloudfront.

The ACM cert must be in us-east-1 to use this module.


Be sure to point your route53 dns records for foo.io A record alias to your cloudfront distribution.
Add add a CNAME record for www.foo.io to point to foo.io

You have to add a policy to your s3 bucket that holds your website files.
```
{
    "Version": "2008-10-17",
    "Id": "PolicyForCloudFrontPrivateContent",
    "Statement": [
        {
            "Sid": "1",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity <Cloudfront ID>"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::<bucket-name>/*"
        }
    ]
}
```

### inputs
- aliases (list, default = null): list of aliases for the cloudfront distribution
- bucket_name (string): name of the bucket that holds the website files
- bucket_regional_domain_name (string): s3 bucket regional domain name
- log_bucket_name (string): which bucket to send the access logs to
- enabled (bool, default = true): enable/disable this cloudfront distribution
- acm_certificate_arn (string): arn of the acm cert to use for this distribution (must be in us-east-1)
- comment (string): cloudfront origin access identity name
- minimum_protocol_version (string, default = "TLSv1.1_2018"): which ssl protocol to use
- lambda_function_association (list(map), default = []): list of lambdas@edge to attach to this cloudfront website
 
### outputs
- domain_name (string): cloudfront domain name
- hosted_zone_id (string): cloudfront hosted zone id

### example
```
module "addyman_io_website" {
  source  = "../../../modules/aws/cloudfront-website"

  bucket_name                 = module.addyman_io.bucket_id
  bucket_regional_domain_name = module.addyman_io.bucket_regional_domain_name
  log_bucket_name             = module.kac_logs.bucket_id
  comment                     = module.addyman_io.bucket_id

  aliases             = ["addyman.io", "www.addyman.io"]
  acm_certificate_arn = module.addyman_io_cert.cert_arn

  lambda_function_association = [{
    event_type   = "origin-response"
    include_body = "false"
    lambda_arn   = module.http_security_headers_addyman_io.qualified_arn
  }]
}

# example of attaching DNS to cloudfront website (NOT REQUIRED, JUST INFORMATIONAL)
resource "aws_route53_record" "addyman_io_website_dns" {
  name    = "addyman.io"
  type    = "A"
  zone_id = data.terraform_remote_state.global.outputs.addyman_io_hosted_zone_id

  alias {
    evaluate_target_health = false
    name                   = module.addyman_io_website.domain_name
    zone_id                = module.addyman_io_website.hosted_zone_id
  }
}
```

# api-gateway-doamin

This will setup a custom domain name for api gateways V2.

### inputs
- domain_name (string): fqdn for the new domain name
- acm_cert_arn (string): arn for the regional acm cert to use for the domain
- base_path_mappings (string): api_id and stage_name
 
### outputs
- domain_name (string): api gateawy domain id
- zone_id (string): api gateway zone id

### example
```
module "prd_api_codetest_com_domain_name" {
  source       = "../../../modules/aws/api-gateway-domainv2"
  domain_name  = "api.codetest.com"
  acm_cert_arn = module.codetest_com_cert.cert_arn

  base_path_mappings = {
    "y8razzwtqc:$default" = {
      api_id     = "y8razzwtqc"
      stage_name = "$default"
    }
  }
}
```

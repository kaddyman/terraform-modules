variable domain_name {}
variable acm_cert_arn {}
variable base_path_mappings {}
variable security_policy { default = "TLS_1_2" }

locals {
  default_tags = {
    terralign = "true"
  }
}

resource "aws_apigatewayv2_domain_name" "self" {
  domain_name              = var.domain_name

  domain_name_configuration {
    certificate_arn = var.acm_cert_arn
    endpoint_type   = "REGIONAL"
    security_policy = var.security_policy
  }

  tags = local.default_tags
}

output "domain_name" {
  value = aws_apigatewayv2_domain_name.self.domain_name_configuration[0].target_domain_name
}

output "zone_id" {
  value = aws_apigatewayv2_domain_name.self.domain_name_configuration[0].hosted_zone_id
}

resource "aws_apigatewayv2_api_mapping" "self" {
  for_each    = var.base_path_mappings
  api_id      = each.value["api_id"]
  domain_name = aws_apigatewayv2_domain_name.self.id
  stage       = each.value["stage_name"]
  api_mapping_key = each.value["api_mapping_key"]
}

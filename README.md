# Terraform 1.0.9 Modules

This is a small sample of terraform modules I have written. 

---

### bin

- [bootstrap](bin/bootstrap): python,create a role for terraform to use
- [taskip](bin/taskip): bash, find public of a public fargate service

---

### Module List

- [acm-cert](aws/acm-cert/README.md)
- [api-gateway-domain](aws/api-gateway-domain/README.md)
- [api-gatewayv2-domain](aws/api-gatewayv2-domain/README.md)
- [api-gateway-lambda](aws/api-gateway-lambda/README.md)
- [api-gatewayv2-lambda](aws/api-gatewayv2-lambda/README.md)
- [api-gateway-logging-role](aws/api-gateway-logging-role/README.md)
- [api-gateway-usage-key](aws/api-gateway-usage-key/README.md)
- [aws-sftp](aws/aws-sftp/README.md)
- [cloudwatch-group](aws/cloudwatch-group/README.md)
- [cloudfront-website](aws/cloudfront-website/README.md)
- [dynamo-db](aws/dynamo-db/README.md)
- [ecs-cluster-fargate](aws/ecs-cluster-fargate/README.md)
- [ecs-service-fargate](aws/ecs-service-fargate/README.md)
- [http-security-headers](aws/http-security-headers/README.md)
- [iam-group](aws/iam-group/README.md)
- [iam-mfa](aws/iam-mfa/README.md)
- [iam-role](aws/iam-role/README.md)
- [iam-service-account](aws/iam-service-account/README.md)
- [lambda](aws/lambda/README.md)
- [network-fargate](aws/network-fargate/README.md)
- [route53-zone](aws/route53-zone/README.md)
- [s3-bucket](aws/s3-bucket/README.md)
- [ses-domain](aws/ses-domain/README.md)
- [ses-emails](aws/ses-emails/README.md)
